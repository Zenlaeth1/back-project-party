package com.exo2.Exercice2.controller;

import com.exo2.Exercice2.dto.VideoGameDto;
import com.exo2.Exercice2.entity.VideoGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.exo2.Exercice2.service.VideoGameService;

import java.util.List;

@RestController
@RequestMapping("/videogames")
public class VideoGameController {

    @Autowired
    private VideoGameService videoGameService;

    @GetMapping
    public ResponseEntity<Page<VideoGameDto>> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(videoGameService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<VideoGameDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(videoGameService.findById(id));
    }

    @PostMapping
    public ResponseEntity<VideoGameDto> save(@RequestBody VideoGameDto videoGame) {
        return ResponseEntity.ok(videoGameService.save(videoGame));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VideoGameDto> update(@PathVariable Long id, @RequestBody VideoGameDto videoGame) {
        return ResponseEntity.ok(videoGameService.update(id, videoGame));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        videoGameService.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
