package com.exo2.Exercice2.controller;


import com.exo2.Exercice2.dto.BoardGameDto;
import com.exo2.Exercice2.entity.BoardGame;
import com.exo2.Exercice2.service.BoardGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/boardgames")
public class BoardGameController {

    @Autowired
    private BoardGameService boardGameService;

    @GetMapping
    public ResponseEntity<Page<BoardGameDto>> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(boardGameService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BoardGameDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(boardGameService.findById(id));
    }

    @PostMapping
    public ResponseEntity<BoardGameDto> save(@RequestBody BoardGameDto boardGame) {
        return ResponseEntity.ok(boardGameService.save(boardGame));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BoardGameDto> update(@RequestBody BoardGameDto boardGame) {
        return ResponseEntity.ok(boardGameService.update(boardGame));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        boardGameService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
