package com.exo2.Exercice2.controller;


import com.exo2.Exercice2.dto.SnackDto;
import com.exo2.Exercice2.entity.Snack;
import com.exo2.Exercice2.service.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/snacks")
public class SnackController {

    @Autowired
    private SnackService snackService;

    @GetMapping
    public ResponseEntity<Page<SnackDto>> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(snackService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<SnackDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(snackService.findById(id));
    }

    @PostMapping
    public ResponseEntity<SnackDto> save(@RequestBody SnackDto snack) {
        return ResponseEntity.ok(snackService.save(snack));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SnackDto> update(@PathVariable Long id, @RequestBody SnackDto snack) {
        return ResponseEntity.ok(snackService.update(id, snack));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        snackService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
