package com.exo2.Exercice2.controller;

import com.exo2.Exercice2.dto.UserDto;
import com.exo2.Exercice2.dto.request.SignIn;
import com.exo2.Exercice2.dto.request.SignUp;
import com.exo2.Exercice2.dto.response.JwtResponse;
import com.exo2.Exercice2.entity.User;
import com.exo2.Exercice2.service.UserService;
import com.exo2.Exercice2.service.security.JwtService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserController(
            UserService userService,
            AuthenticationManager authenticationManager,
            JwtService jwtService,
            BCryptPasswordEncoder passwordEncoder
    ) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/sign-in")
    public ResponseEntity<JwtResponse> signIn(@RequestBody SignIn requestBody) {
        if (userService.existsByEmail(requestBody.email())) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            requestBody.email(),
                            requestBody.password()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtService.createToken(authentication.getName());

            return ResponseEntity.ok(JwtResponse.from(token));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping("/sign-up")
    public ResponseEntity<String> signUp(@RequestBody SignUp requestBody) {
        try {
            if (!userService.existsByEmail(requestBody.email())) {
                User user = User.builder()
                        .withEmail(requestBody.email())
                        .withPassword(passwordEncoder.encode(requestBody.password()))
                        .withName(requestBody.name())
                        .withAge(requestBody.age())
                        .withCity(requestBody.city())
                        .withInterests(requestBody.interests())
                        .build();
                userService.save(user);

                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().body("Error while trying to register user.");
            }
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findById(@PathVariable Long id)
    {
        return ResponseEntity.ok(userService.findById(id));
    }
}
