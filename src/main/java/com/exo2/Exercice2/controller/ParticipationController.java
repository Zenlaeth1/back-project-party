package com.exo2.Exercice2.controller;


import com.exo2.Exercice2.dto.ParticipationDto;
import com.exo2.Exercice2.entity.Participation;
import com.exo2.Exercice2.service.ParticipationService;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/participations")
public class ParticipationController {

    @Autowired
    private ParticipationService participationService;

    @GetMapping
    public ResponseEntity<Page<ParticipationDto>> findAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(participationService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParticipationDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(participationService.findById(id));
    }

    @PostMapping
    public ResponseEntity<ParticipationDto> save(@RequestBody ParticipationDto participation) {
        try {
            return ResponseEntity.ok(participationService.save(participation));
        } catch (BadRequestException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ParticipationDto> update( @RequestBody ParticipationDto participation) {
        return ResponseEntity.ok(participationService.update(participation));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        participationService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
