package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.SnackDto;
import com.exo2.Exercice2.entity.Snack;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SnackMapper {
    // Single Object
    Snack toEntity(SnackDto snackDto);
    SnackDto toDto(Snack snack);
    // List of Objects
    List<Snack> toEntities(List<SnackDto> snacksDto);
    List<SnackDto> toDtos(List<Snack> snacks);
}