package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.ParticipationDto;
import com.exo2.Exercice2.entity.Participation;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ParticipationMapper {
    // Single Object
    Participation toEntity(ParticipationDto participationDto);
    ParticipationDto toDto(Participation participation);
    // List of Objects
    List<Participation> toEntities(List<ParticipationDto> participationsDto);
    List<ParticipationDto> toDtos(List<Participation> participations);
}