package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.VideoGameDto;
import com.exo2.Exercice2.entity.VideoGame;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VideoGameMapper {
    // Single Object
    VideoGame toEntity(VideoGameDto videoGameDto);
    VideoGameDto toDto(VideoGame videoGame);
    // List of Objects
    List<VideoGame> toEntities(List<VideoGameDto> videoGamesDto);
    List<VideoGameDto> toDtos(List<VideoGame> videoGames);
}