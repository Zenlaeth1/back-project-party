package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.BoardGameDto;
import com.exo2.Exercice2.entity.BoardGame;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BoardGameMapper {
    // Single Object
    BoardGame toEntity(BoardGameDto boardGameDto);
    BoardGameDto toDto(BoardGame boardGame);
    // List of Objects
    List<BoardGame> toEntities(List<BoardGameDto> boardGamesDto);
    List<BoardGameDto> toDtos(List<BoardGame> boardGames);
}