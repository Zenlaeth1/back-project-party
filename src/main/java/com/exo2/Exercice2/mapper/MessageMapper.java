package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.MessageDto;
import com.exo2.Exercice2.entity.Message;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageMapper {
    // Single Object
    Message toEntity(MessageDto messageDto);
    MessageDto toDto(Message message);
    // List of Objects
    List<Message> toEntities(List<MessageDto> messagesDto);
    List<MessageDto> toDtos(List<Message> messages);
}