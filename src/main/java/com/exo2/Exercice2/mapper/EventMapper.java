package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.EventDto;
import com.exo2.Exercice2.entity.Event;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EventMapper {
    // Single Object
    Event toEntity(EventDto eventDto);
    EventDto toDto(Event event);
    // List of Objects
    List<Event> toEntities(List<EventDto> eventsDto);
    List<EventDto> toDtos(List<Event> events);
}