package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.CommentDto;
import com.exo2.Exercice2.entity.Comment;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {
    // Single Object
    Comment toEntity(CommentDto commentDto);
    CommentDto toDto(Comment comment);
    // List of Objects
    List<Comment> toEntities(List<CommentDto> commentsDto);
    List<CommentDto> toDtos(List<Comment> comments);
}