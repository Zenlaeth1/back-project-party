package com.exo2.Exercice2.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String name;
    private String location;
    private String eventType;
    private Date dateTime;
    private int maxSpots;
    private boolean isPaid;
    private float price;
    private String details;
    private boolean requestBoardGames;
    private boolean requestVideoGames;
    private boolean requestSnacks;
}