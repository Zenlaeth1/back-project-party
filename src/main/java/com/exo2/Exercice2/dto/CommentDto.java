package com.exo2.Exercice2.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private float rating;
    private String commentText;
    private Date createdAt;
    private int userId;
    private int eventId;
}