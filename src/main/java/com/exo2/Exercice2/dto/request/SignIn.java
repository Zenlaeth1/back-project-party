package com.exo2.Exercice2.dto.request;

public record SignIn(
        String email,
        String password
) {
}