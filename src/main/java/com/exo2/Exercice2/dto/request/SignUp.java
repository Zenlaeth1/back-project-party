package com.exo2.Exercice2.dto.request;

public record SignUp(
        String email,
        String password,
        String name,
        int age,
        String city,
        String interests
) {
}