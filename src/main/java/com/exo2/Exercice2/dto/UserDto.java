package com.exo2.Exercice2.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private int id;
    private String email;
    private String name;
    private int age;
    private String city;
    private String interests;
    private float rating;
}
