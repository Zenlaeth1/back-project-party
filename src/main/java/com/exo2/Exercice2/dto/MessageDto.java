package com.exo2.Exercice2.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String messageText;
    private Date sentAt;
    private int senderId;
    private int receiverId;
    private int eventId;
}