package com.exo2.Exercice2.dto.response;

public record JwtResponse(
        String accessToken
) {
    public static JwtResponse from(String token) {
        return new JwtResponse(token);
    }

    public static JwtResponse empty() {
        return new JwtResponse(null);
    }
}
