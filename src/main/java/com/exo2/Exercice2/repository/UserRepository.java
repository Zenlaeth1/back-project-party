package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User save(User iamUser);

    Optional<User> findByEmail(String email);

    boolean existsByEmail(String email);
}