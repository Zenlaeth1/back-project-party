package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Adresse;
import com.exo2.Exercice2.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findByName(String name);
}
