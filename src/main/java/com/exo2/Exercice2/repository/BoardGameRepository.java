package com.exo2.Exercice2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.exo2.Exercice2.entity.BoardGame;


public interface BoardGameRepository extends JpaRepository<BoardGame, Long>{
}
