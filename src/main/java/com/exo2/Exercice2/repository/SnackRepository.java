package com.exo2.Exercice2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.exo2.Exercice2.entity.Snack;

public interface SnackRepository extends JpaRepository<Snack, Long>{
}
