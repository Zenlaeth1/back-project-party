package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long>{
}
