package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Event;
import com.exo2.Exercice2.entity.Participation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParticipationRepository extends JpaRepository<Participation, Long>{
    Participation findOneParticipationByEventIdAndUserId(int eventId, int userId);
}
