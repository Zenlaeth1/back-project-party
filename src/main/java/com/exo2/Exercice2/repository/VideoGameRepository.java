package com.exo2.Exercice2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.exo2.Exercice2.entity.VideoGame;

public interface VideoGameRepository extends JpaRepository<VideoGame, Long>{
}
