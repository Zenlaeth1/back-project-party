package com.exo2.Exercice2.repository;

import com.exo2.Exercice2.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long>{
}
