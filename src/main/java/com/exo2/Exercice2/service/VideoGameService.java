package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.VideoGameDto;
import com.exo2.Exercice2.entity.VideoGame;
import com.exo2.Exercice2.mapper.VideoGameMapper;
import com.exo2.Exercice2.repository.VideoGameRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class VideoGameService {

    private VideoGameRepository videoGameRepository;
    private VideoGameMapper videoGameMapper;

    public Page<VideoGameDto> findAll(Pageable pageable) {
        return videoGameRepository.findAll(pageable).map(videoGameMapper::toDto);
    }

    public VideoGameDto findById(Long id) {
        return videoGameMapper.toDto(videoGameRepository.findById(id).orElse(null));
    }

    public VideoGameDto save(VideoGameDto videoGameDto) {
        return videoGameMapper.toDto(videoGameRepository.save(videoGameMapper.toEntity(videoGameDto)));
        }

    public void deleteById(Long id) {
        videoGameRepository.deleteById(id);
    }

    public VideoGameDto update(Long id, VideoGameDto videoGameDto) {
        return videoGameRepository.findById(id)
                .map(existingVideoGame -> {
                    VideoGame videoGame = videoGameMapper.toEntity(videoGameDto);
                    if (Objects.nonNull(existingVideoGame.getName())) {
                        videoGame.setName(videoGameDto.getName());
                    }
                    if(Objects.nonNull(existingVideoGame.getPlatform())) {
                        videoGame.setPlatform(videoGameDto.getPlatform());
                    }
                    return videoGameMapper.toDto(videoGameRepository.save(videoGame));
                })
                .orElse(null);
    }

}
