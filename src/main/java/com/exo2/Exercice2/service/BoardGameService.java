package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.BoardGameDto;
import com.exo2.Exercice2.mapper.BoardGameMapper;
import com.exo2.Exercice2.repository.BoardGameRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BoardGameService {

    private BoardGameRepository boardGameRepository;
    private BoardGameMapper boardGameMapper;

    public Page<BoardGameDto> findAll(Pageable pageable) {
        return boardGameRepository.findAll(pageable).map(boardGameMapper::toDto);
    }

    public BoardGameDto findById(Long id) {
        return boardGameMapper.toDto(boardGameRepository.findById(id).orElse(null));
    }

    public BoardGameDto save(BoardGameDto boardGameDto) {
        return boardGameMapper.toDto(boardGameRepository.save(boardGameMapper.toEntity(boardGameDto)));
    }

    public void deleteById(Long id) {
        boardGameRepository.deleteById(id);
    }

    public BoardGameDto update(BoardGameDto boardGameDto) {
        return boardGameMapper.toDto(boardGameRepository.save(boardGameMapper.toEntity(boardGameDto)));
    }
}
