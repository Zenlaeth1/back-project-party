package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EventDto;
import com.exo2.Exercice2.entity.Event;
import com.exo2.Exercice2.mapper.EventMapper;
import com.exo2.Exercice2.repository.EventRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EventService {

    private EventRepository eventRepository;
    private EventMapper eventMapper;

    @Cacheable("event")
    public Page<EventDto> findAll(Pageable pageable) {

        return eventRepository.findAll(pageable).map(eventMapper::toDto);
    }

    public EventDto findById(Long id) {
        return eventMapper.toDto(eventRepository.findById(id).orElse(null));
    }

    @CacheEvict(value = "event", allEntries = true)
    public EventDto save(EventDto eventDto) {
        Event event = eventMapper.toEntity(eventDto);
        event.setRemainingSpots(event.getMaxSpots());

        return eventMapper.toDto(eventRepository.save(event));
    }

    @CacheEvict(value = "event", allEntries = true)
    public void deleteById(Long id) {
        eventRepository.deleteById(id);
    }

    @CacheEvict(value = "event", allEntries = true)
    public EventDto update(Long id, EventDto eventDto) {

        return eventRepository.findById(id)
                .map(existingEvent -> {
                    Event event = eventMapper.toEntity(eventDto);
                    return eventMapper.toDto(eventRepository.save(event));
                })
                .orElse(null);

    }
}
