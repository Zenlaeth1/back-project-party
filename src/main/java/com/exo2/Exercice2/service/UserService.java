package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.UserDto;
import com.exo2.Exercice2.entity.User;
import com.exo2.Exercice2.mapper.UserMapper;
import com.exo2.Exercice2.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public UserDto findById(Long id) {
        return userMapper.toDto(userRepository.findById(id).orElse(null));
    }

    public Optional<UserDto> findByEmail(String email) {
        return Optional.ofNullable(userMapper.toDto(
                userRepository.findByEmail(email)
                        .orElseThrow(() -> new UsernameNotFoundException(email)))
        );
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
}