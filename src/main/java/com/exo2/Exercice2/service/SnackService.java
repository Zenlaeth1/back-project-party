package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.SnackDto;
import com.exo2.Exercice2.entity.Snack;
import com.exo2.Exercice2.mapper.SnackMapper;
import com.exo2.Exercice2.repository.SnackRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SnackService {

    private SnackRepository snackRepository;
    private SnackMapper snackMapper;

    public Page<SnackDto> findAll(Pageable pageable) {
        return snackRepository.findAll(pageable).map(snackMapper::toDto);
    }

    public SnackDto findById(Long id) {
        return snackMapper.toDto(snackRepository.findById(id).orElse(null));
    }

    public SnackDto save(SnackDto snackDto) {
        return snackMapper.toDto(snackRepository.save(snackMapper.toEntity(snackDto)));
    }

    public void deleteById(Long id) {
        snackRepository.deleteById(id);
    }

    public SnackDto update(Long id, SnackDto SnackDto) {

        return snackRepository.findById(id)
                .map(existingSnack -> {
                    Snack snack = snackMapper.toEntity(SnackDto);
                    return snackMapper.toDto(snackRepository.save(snack));
                })
                .orElse(null);
    }
}
