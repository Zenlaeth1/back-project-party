package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.ParticipationDto;
import com.exo2.Exercice2.entity.Event;
import com.exo2.Exercice2.entity.Participation;
import com.exo2.Exercice2.entity.User;
import com.exo2.Exercice2.mapper.ParticipationMapper;
import com.exo2.Exercice2.repository.EventRepository;
import com.exo2.Exercice2.repository.ParticipationRepository;
import com.exo2.Exercice2.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParticipationService {

    private final UserRepository userRepository;
    private final EventRepository eventRepository;
    private ParticipationRepository participationRepository;
    private ParticipationMapper participationMapper;

    public Page<ParticipationDto> findAll(Pageable pageable) {
        return participationRepository.findAll(pageable).map(participationMapper::toDto);
    }

    public ParticipationDto findById(Long id) {
        return participationMapper.toDto(participationRepository.findById(id).orElse(null));
    }

    public ParticipationDto save(ParticipationDto participationDto) throws BadRequestException {
        Participation existingParticipation = participationRepository.findOneParticipationByEventIdAndUserId(participationDto.getEventId(), participationDto.getUserId());
        if (existingParticipation != null) {
            throw new org.apache.coyote.BadRequestException("Participation already exists");
        } else {
            Participation participation = participationMapper.toEntity(participationDto);

            Optional<Event> eventOptional = eventRepository.findById((long) participationDto.getEventId());
            if (eventOptional.isEmpty()) {
                throw new org.apache.coyote.BadRequestException("Event not found");
            }
            participation.setEvent(eventOptional.get());

            Optional<User> userOptional = userRepository.findById((long) participationDto.getUserId());
            if (userOptional.isEmpty()) {
                throw new org.apache.coyote.BadRequestException("User not found");
            }
            participation.setUser(userOptional.get());

            participation.getEvent().participate();

            return participationMapper.toDto(participationRepository.save(participation));
        }
    }

    public void deleteById(Long id) {
        participationRepository.deleteById(id);
    }

    public ParticipationDto update(ParticipationDto participationDto) {
        return participationMapper.toDto(participationRepository.save(participationMapper.toEntity(participationDto)));
    }
}
