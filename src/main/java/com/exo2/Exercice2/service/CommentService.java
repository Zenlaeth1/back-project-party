package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.CommentDto;
import com.exo2.Exercice2.entity.Comment;
import com.exo2.Exercice2.mapper.CommentMapper;
import com.exo2.Exercice2.repository.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CommentService {

    private CommentRepository commentRepository;
    private CommentMapper commentMapper;

    public Page<CommentDto> findAll(Pageable pageable) {
        return commentRepository.findAll(pageable).map(commentMapper::toDto);
    }

    public CommentDto findById(Long id) {
        return commentMapper.toDto(commentRepository.findById(id).orElse(null));
    }

    public CommentDto save(CommentDto commentDto) {
        return commentMapper.toDto(commentRepository.save(commentMapper.toEntity(commentDto)));
    }

    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }

    public CommentDto update(CommentDto commentDto) {
        return commentMapper.toDto(commentRepository.save(commentMapper.toEntity(commentDto)));
    }

}
