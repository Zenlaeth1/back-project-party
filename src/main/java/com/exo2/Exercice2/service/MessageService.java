package com.exo2.Exercice2.service;


import com.exo2.Exercice2.dto.MessageDto;
import com.exo2.Exercice2.entity.Message;
import com.exo2.Exercice2.mapper.MessageMapper;
import com.exo2.Exercice2.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {

    private MessageRepository messageRepository;
    private MessageMapper messageMapper;

    public Page<MessageDto> findAll(Pageable pageable) {
        return messageRepository.findAll(pageable).map(messageMapper::toDto);
    }

    public MessageDto findById(Long id) {
        return messageMapper.toDto(messageRepository.findById(id).orElse(null));
    }

    public MessageDto save(MessageDto messageDto) {
        return messageMapper.toDto(messageRepository.save(messageMapper.toEntity(messageDto)));
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

    public MessageDto update(Long id, MessageDto messageDto) {

            return messageRepository.findById(id)
                    .map(existingMessage -> {
                        Message message = messageMapper.toEntity(messageDto);
                        return messageMapper.toDto(messageRepository.save(message));
                    })
                    .orElse(null);
    }
}
