package com.exo2.Exercice2.entity;

import java.util.Date;
import java.util.List;

import com.exo2.Exercice2.dto.EventDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import java.util.List;

@Entity
@Table(name = "event", indexes = @Index(name = "idx_name", columnList = "name"))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String location;
    @Enumerated(EnumType.STRING)
    private EventType eventType;
    private Date dateTime;
    private int remainingSpots;
    private int maxSpots;
    private boolean isPaid;
    private float price;
    private String details;
    private boolean requestBoardGames;
    private boolean requestVideoGames;
    private boolean requestSnacks;

    @ManyToOne
    @JoinColumn(name = "organizer_id")
    private User organizer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<Participation> participations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<Comment> comments;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<Message> messages;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<BoardGame> boardGames;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<VideoGame> videoGames;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    @BatchSize(size = 10)
    private List<Snack> snacks;

    public void participate() {
        this.setRemainingSpots(this.getRemainingSpots() - 1);
    }

    public void cancel() {
        this.setRemainingSpots(this.getRemainingSpots() + 1);
    }
}

