package com.exo2.Exercice2.entity;

public enum EventType {
    JEUX_DE_SOCIETE("Jeux de société"),
    LAN("LAN"),
    CLASSIQUE("Classique");

    private String label;

    EventType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}