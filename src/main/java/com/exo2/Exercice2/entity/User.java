package com.exo2.Exercice2.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.util.List;

@Entity
@Table(name = "`users`")
@Builder(setterPrefix = "with")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String email;
    private String password;
    private String name;
    private int age;
    private String city;
    private String interests;
    private float rating;

    @OneToMany(mappedBy = "organizer")
    private List<Event> organizedEvents;

    @OneToMany(mappedBy = "user")
    @BatchSize(size = 10)
    private List<Participation> participations;

    @OneToMany(mappedBy = "user")
    @BatchSize(size = 10)
    private List<Comment> comments;

    @OneToMany(mappedBy = "sender")
    @BatchSize(size = 10)
    private List<Message> sentMessages;

    @OneToMany(mappedBy = "receiver")
    @BatchSize(size = 10)
    private List<Message> receivedMessages;

    @OneToMany(mappedBy = "broughtBy")
    @BatchSize(size = 10)
    private List<BoardGame> boardGames;

    @OneToMany(mappedBy = "broughtBy")
    @BatchSize(size = 10)
    private List<VideoGame> videoGames;

    @OneToMany(mappedBy = "broughtBy")
    @BatchSize(size = 10)
    private List<Snack> snacks;

}
